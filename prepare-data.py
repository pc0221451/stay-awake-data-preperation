#read through all folds 
				#For each fold
				#    For each drowsiness level
				#		Convert video to pictures
				#For each fold of extracted images, crop each picture of just face 
				#
				#For each fold	
				#	Extract dlib points and put into csv
				#For each fold, append csv to each other, creating 1 large csv for 
				#Each fold.
#


# facial landmarks 

"""

left eye --> [42,47]
mouth --> [48,67]
left eyebrow --> [22,26]
nose --> [27,34]
right eyebrow --> [17,21]
right eye --> [36,41]
jaw --> [0,16]

"""
import csv
import glob as glob
from importlib.resources import path
from turtle import pu
import cv2
from imutils import face_utils
import imutils
import os
from skimage import io
import numpy as np
import dlib
from PIL import Image
import matplotlib.pyplot as plt
from scipy.spatial import distance as dist
import math
from sklearn.preprocessing import normalize #machine learning algorithm library
from pandas import read_csv
from sklearn import preprocessing



def eye_aspect_ratio(eye):
    p2_minus_p6 = dist.euclidean(eye[1], eye[5])
    p3_minus_p5 = dist.euclidean(eye[2], eye[4])
    p1_minus_p4 = dist.euclidean(eye[0], eye[3])
    ear = (p2_minus_p6 + p3_minus_p5) / (2.0 * p1_minus_p4)
    return ear

def mouth_aspect_ratio(mouth):
	# compute the euclidean distances between the two sets of
	# vertical mouth landmarks (x, y)-coordinates
	A = dist.euclidean(mouth[2], mouth[10]) # 51, 59
	B = dist.euclidean(mouth[4], mouth[8]) # 53, 57

	# compute the euclidean distance between the horizontal
	# mouth landmark (x, y)-coordinates
	C = dist.euclidean(mouth[0], mouth[6]) # 49, 55

	# compute the mouth aspect ratio
	mar = (A + B) / (2.0 * C)

	# return the mouth aspect ratio
	return mar

def cal_PUC(arr):
    distance_1_2 = math.sqrt(pow(arr[0][0] - arr[1][0], 2) + pow(arr[0][1] - arr[1][1], 2))
    distance_2_3 = math.sqrt(pow(arr[1][0] - arr[2][0], 2) + pow(arr[1][1] - arr[2][1], 2))
    distance_3_4 = math.sqrt(pow(arr[2][0] - arr[3][0], 2) + pow(arr[2][1] - arr[3][1], 2))
    distance_4_5 = math.sqrt(pow(arr[3][0] - arr[4][0], 2) + pow(arr[3][1] - arr[4][1], 2))
    distance_5_6 = math.sqrt(pow(arr[4][0] - arr[5][0], 2) + pow(arr[4][1] - arr[5][1], 2))
    distance_6_1 = math.sqrt(pow(arr[5][0] - arr[0][0], 2) + pow(arr[5][1] - arr[0][1], 2))
    distance_2_5 = math.sqrt(pow(arr[2][0] - arr[5][0], 2) + pow(arr[2][1] - arr[5][1], 2))
    perimeter = distance_1_2 + distance_2_3 + distance_3_4 + distance_4_5 + distance_5_6 + distance_6_1
    area = pow(distance_2_5 / 2, 2) * math.pi
    circularity = (4 * math.pi * area) / pow(perimeter, 2)
    return circularity


face_detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor("shape_predictor_68_face_landmarks.dat")

# Finding landmark id for left and right eyes
(leftEyeStart, leftEyeEnd) = face_utils.FACIAL_LANDMARKS_IDXS["left_eye"]
(rightEyeStart, rightEyeEnd) = face_utils.FACIAL_LANDMARKS_IDXS["right_eye"]
(mstart, mEnd) = face_utils.FACIAL_LANDMARKS_IDXS["mouth"]



# get locations of all training data raw videos
participantLocation = glob.glob(r'/Volumes/My Passport/stay-awake-data/facial-landmarks-net/raw-videos/*/*', recursive=True)



# create corresponding stills of each video, and save them in desired location

for participants in participantLocation:
    participantClass = glob.glob(r'{path}/*'.format(path=participants), recursive=True)

    
    for video in participantClass:

        splitLocation = os.path.normpath(video)
        saveLocation = splitLocation.split(os.sep)
        splitArray = saveLocation[-3:]
        # address of where to create / save segmented pictures
        saveAddress = splitArray[0] + '/' + splitArray[1] + '/' + splitArray[2]
        
        vidcap = cv2.VideoCapture(video)
        path = '/Volumes/My Passport/stay-awake-data/facial-landmarks-net/raw-images/{path}/'.format(path = saveAddress[:-4])
        print(path)
        if not os.path.exists(path):
            os.makedirs(path)
        
            def getFrame(sec):
                vidcap.set(cv2.CAP_PROP_POS_MSEC,sec*1000)
                hasFrames,image = vidcap.read()
                if hasFrames:
                    cv2.imwrite(path + "image"+str(count)+".jpg", image)
                    print("generated :--> image." + str(count) + "jpg")    
                return hasFrames
            sec = 0
            frameRate = 0.5 #//it will capture image in each 0.5 second
            count=1
            success = getFrame(sec)
            while success:
                count = count + 1
                sec = sec + frameRate
                sec = round(sec, 2)
                success = getFrame(sec)
        
        if os.path.exists(path):
            print('path already exists - stills have been created')



# for each collection of stills, crop the pics to outline the face using dLib

stillsLocation = glob.glob(r'/Volumes/My Passport/stay-awake-data/facial-landmarks-net/raw-images/*/*/*/*.jpg', recursive=True)

crop_width = 540
simple_crop = True


for file in stillsLocation:
    
    img = plt.imread(file)
    detected_faces = face_detector(img, 1)

    for i, face_rect in enumerate(detected_faces):
        width = face_rect.right() - face_rect.left()
        height = face_rect.bottom() - face_rect.top()
    
        image_to_crop = Image.open(file)
            
        if simple_crop:
            crop_area = (face_rect.left(), face_rect.top(), face_rect.right(), face_rect.bottom())
        else:
            size_array = []
            size_array.append(face_rect.top())
            size_array.append(image_to_crop.height - face_rect.bottom())
            size_array.append(face_rect.left())
            size_array.append(image_to_crop.width - face_rect.right())
            size_array.sort()
            short_side = size_array[0]
            crop_area = (face_rect.left() - size_array[0] , face_rect.top() - size_array[0], face_rect.right() + size_array[0], face_rect.bottom() + size_array[0])
            
        cropped_image = image_to_crop.crop(crop_area)
        crop_size = (crop_width, crop_width)
        cropped_image.thumbnail(crop_size)
        cropped_image.save(file)
        print("cropped :-> " + file)

# for each fold, extract cooordinates, and write to master fold.csv

croppedStillsLocations = glob.glob(r'/Volumes/My Passport/stay-awake-data/facial-landmarks-net/raw-images/*/*/*', recursive=True)

for croppedStill in croppedStillsLocations:
    for file in glob.glob(r'{path}/*.jpg'.format(path = croppedStill), recursive=True):
        image = cv2.imread(file)
        gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
        # detect faces in the grayscale image
        rects = face_detector(gray, 1)
        # loop over the face detections
        for (i, rect) in enumerate(rects):
            
            # determine the facial landmarks for the face region, then
            # convert the facial landmark (x, y)-coordinates to a NumPy
            # array
            
            shape = predictor(gray, rect)
            shape = face_utils.shape_to_np(shape)

            # calculate eye aspect ratio

            leftEye = shape[leftEyeStart:leftEyeEnd]
            rightEye = shape[rightEyeStart:rightEyeEnd]

            leftEAR = eye_aspect_ratio(leftEye)
            rightEAR = eye_aspect_ratio(rightEye)
            
            ear = (leftEAR + rightEAR) / 2.0

            # calculate mouth aspect ratio

            mouth=shape[mstart:mEnd]
            mouthAR= mouth_aspect_ratio(mouth)
            mar=mouthAR
          
            # calculate Pupil Circularity

            leftPUC = cal_PUC(leftEye)
            rightPUC = cal_PUC(rightEye)

            puc = (leftPUC + rightPUC) / 2

    
            # calculate Mouth aspect ratio over Eye aspect ratio

            splitLocation = os.path.normpath(croppedStill)
            saveLocation = splitLocation.split(os.sep)
            splitArray = saveLocation[-3:]
            
            # address of where to create / save segmented pictures
            saveAddress = splitArray[0] + '/' + splitArray[1] + '/'
            
            rawCoordinates = []
            # append classification to start of array / csv
            rawCoordinates.append(splitArray[2])
            # append eye aspect ratio to csv
            rawCoordinates.append(ear)
            # append mouth aspect ratio
            rawCoordinates.append(mar)
            # append PUC
            rawCoordinates.append(puc)

            #rawCoordinates.append(file)


            """
            for (x, y) in shape:
                rawCoordinates.append(x)
                rawCoordinates.append(y)
            """
            
        
        # convert numPy array to csv to be written to file
        converted_list = [str(element) for element in rawCoordinates]
        joined_string = ",".join(converted_list)    
    
        

        csvData = '/Volumes/My Passport/stay-awake-data/facial-landmarks-net/dlib-input-data-coordinates/indavidual-fold-data/{path}'.format(path = saveAddress)
        
        # if csv hasnt already been created
        if not os.path.exists(csvData):
            os.makedirs(csvData)
            print("made new directory :--" + csvData)
        
        file = open("{path}/data.csv".format(path = csvData), "a")
        file.write(joined_string + "\n")
        print("writing to :--> data.csv")
        file.close()

"""

dataframe = read_csv('/Volumes/My Passport/stay-awake-data/facial-landmarks-net/dlib-input-data-coordinates/indavidual-fold-data/{path}/data.csv'.format(path = saveAddress))

x=dataframe.iloc[:,1:].values
y=dataframe.iloc[:,0].values


print("Examples of X\n",x[:])
print("Examples of y\n",y[:])

x_normalized=normalize(x,axis=0)
print("Examples of X_normalised\n",x_normalized[0:3])        



min_max_scaler = preprocessing.MinMaxScaler()
X_train_minmax = min_max_scaler.fit_transform(x)
print("Examples of X_normalised\n",X_train_minmax[0:3])   

# convert numPy array to csv to be written to file
converted_list = [str(element) for element in X_train_minmax]

joined_string = ",".join(converted_list)
        
file = open("{path}/normalized_data.csv".format(path = csvData), "a")
file.write(joined_string + "\n")
print("writing to :--> normalized_data.csv")
file.close()


"""